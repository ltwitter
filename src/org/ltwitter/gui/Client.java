package org.ltwitter.gui;

import java.awt.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import winterwell.jtwitter.*;


public class Client  {
	
	Icon img;
	JFrame loginFrame;
	JLabel lblUser,lblPass,lblStatus,lblHeader;
	JTextField txtUser;
	JPasswordField txtPass;
	JTextArea txtStatus;
	JButton btSubmit,btExit;
	JPanel mainPanel, btPanel;
	BufferedImage imgIcon = null;
	JLabel lblLastStatus;
	JLabel lblStatusCount;
		public void showForm(){
		
			try{
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, "Unable to load system look and feel !");
			}
			
			
			//create, configure and show components
			
			loginFrame = new JFrame("LTwitter");
			lblUser = new JLabel("Enter Username");
			lblPass = new JLabel("Enter Password");
			lblStatus = new JLabel("What are you doing now ? ");
			lblLastStatus = new JLabel();
			
			txtUser = new JTextField(20);
			txtPass = new JPasswordField(20);
			
			txtStatus = new JTextArea(10,10);
			txtStatus.setLineWrap(true);
			txtStatus.getDocument().addDocumentListener(new TweetCountListener());
			JScrollPane scroller = new JScrollPane(txtStatus);
			scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			
			btSubmit = new JButton("Tweet");
			btSubmit.addActionListener(new TweetListener());
			
			
			img = new ImageIcon("images/LTwitter2.jpg");
			lblHeader = new JLabel(img);
			
			
			lblStatusCount = new JLabel(new Integer(140).toString());
			
			
			//creating panels and adding the above components in suitable manner
			
			mainPanel = new JPanel();
			mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
			mainPanel.add(lblUser);
			mainPanel.add(txtUser);
			mainPanel.add(lblPass);
			mainPanel.add(txtPass);
			mainPanel.add(lblStatus);
			mainPanel.add(scroller);
			mainPanel.add(lblStatusCount);
			mainPanel.add(lblLastStatus);
			
			btPanel = new JPanel();
			btPanel.add(btSubmit);
			
			loginFrame.setLayout(new FlowLayout());
			loginFrame.getContentPane().add(lblHeader);
			loginFrame.getContentPane().add(mainPanel);
			loginFrame.getContentPane().add(btPanel);
			
			
			
			
			//centering the frame on the screen
			loginFrame.setSize(300,430);
			Dimension dw = Toolkit.getDefaultToolkit().getScreenSize();
			int wx = loginFrame.getSize().width;
			int wy = loginFrame.getSize().height;
			int x = (dw.width-wx)/2;
			int y = (dw.height-wy)/2;
			loginFrame.setLocation(x, y);
			
			
			
			
			
			loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			loginFrame.setResizable(false);
			loginFrame.setVisible(true);
		
	}
	
	
	
	//button listener updating the status 
        class TweetListener implements ActionListener{
		public void actionPerformed(ActionEvent ev){
			//twitter
			//setting the status of user
			
				
				String user = txtUser.getText();
				char[] pass = txtPass.getPassword();
				
				String password = "";
				for (int i=0;i<pass.length;i++)
					password = password+pass[i];
				
				
				//lblLastStatus.setText("Last tweet:"+txtStatus.getText());
				
				try{
					Twitter twitter = new Twitter(user,password);
					String status = txtStatus.getText();
					
					twitter.setStatus(status);
					
					JOptionPane.showMessageDialog(null, "You just Tweeted!");
					
					txtStatus.setText("");
					txtStatus.requestFocus();
					
					
				}catch(Exception e){
					
					JOptionPane.showMessageDialog(loginFrame, "Twitter Error\n" +
							"Check the username and password or\n" +
							"Twitter may be busy. Try after sometime :)");
				}
		}
	}//inner class
	
	class TweetCountListener implements DocumentListener{
		

		@Override
		public void changedUpdate(DocumentEvent ev) {
			this.manageLength(ev);
			
		}

		@Override
		public void insertUpdate(DocumentEvent ev) {
			
			this.manageLength(ev);
		}

		@Override
		public void removeUpdate(DocumentEvent ev) {
			this.manageLength(ev);
			
		}
		
		public void manageLength(DocumentEvent ev){
			Document doc = ev.getDocument();
			int remLength = 140-doc.getLength();
			
			lblStatusCount.setText("Remaining: "+remLength);
			
			
			
			
			
			
		}
	}
		
}
	

